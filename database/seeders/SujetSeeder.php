<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SujetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sujets')->insert([
            'title' => "Un donuts à voulu manger Homer",
            'status' =>0,
        ]);

        DB::table('sujets')->insert([
            'title' => "2 chats combattent pour un bout de tarte",
            'status' =>0,
        ]);
        DB::table('sujets')->insert([
            'title' => "Faites une battle de danse",
            'status' =>0,
        ]);

        DB::table('sujets')->insert([
            'title' => "Deux familles se croisent au concours de barbecue de Dunkerque",
            'status' =>0,
        ]);
        DB::table('sujets')->insert([
            'title' => "Un groupe imite Trump et l'autre Homer",
            'status' =>0,
        ]);
        DB::table('sujets')->insert([
            'title' => "Une soirée au restaurant",
            'status' =>0,
        ]);
        DB::table('sujets')->insert([
            'title' => "Un chat se dispute avec un chien",
            'status' =>0,
        ]);
        DB::table('sujets')->insert([
            'title' => "Un frére à manger le gouter de sa soeur",
            'status' =>0,
        ]);
        DB::table('sujets')->insert([
            'title' => "Kim Kardashian à piqué un bout de la robe en viande de Lady Gaga",
            'status' =>0,
        ]);
        DB::table('sujets')->insert([
            'title' => "2 collègues de travaillent censés être malades se croisent accidentellement au même entretien d'embauche",
            'status' =>0,
        ]);
        DB::table('sujets')->insert([
            'title' => "2 développeurs parle de PHP",
            'status' =>0,
        ]);
        DB::table('sujets')->insert([
            'title' => "Manon et Pauline doivent faire un choix entre le Coca light et le Coca Zéro",
            'status' =>0,
        ]);
        DB::table('sujets')->insert([
            'title' => "2 chats discutent de leur avenir",
            'status' =>0,
        ]);
    }
}
