# Le carnet des artistes

Lien vers le Trello : 
https://trello.com/b/OQRRrVX7/la-carnet-des-artistes

Lien vers la version en ligne Heroku:
http://fast-springs-36487.herokuapp.com/


# Maquettes

dossier : public/maquettes

# Diagrammess

dossier : public/diagrammes

## Installation

1. composer install

2. créer le fichier .env à partir du fichier .env.example

à modifier : APP_NAME, DB_DATABASE, DB_USERNAME, DB_PASSWORD

3. php artisan key:generate

(Cela devrait remplir le APP_Key du fichier .env)

4. créer la base de données utf8mb4_unicode_ci dans phpmyadmin

5. Faire les migrations et seedings

6. php artisan serve

## Migrations et seedings

1. php artisan migrate:reset
2. php artisan migrate
3. php artisan db:seed
