<?php

namespace App\Http\Controllers;

use App\Models\Sujet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class SujetsController extends Controller
{
    // AFFICHE LES SUJETS DANS LA PAGE D'ACCUEIL
    public function afficheLesSujets(){
        return view('welcome', ['sujets' => Sujet::all()]);
    }
    // AFFICHE UN SEUL SUJET DANS LA PAGE ROULETTE ALEATOIREMENT
    public function afficheLeSujet(){
        return view('roulette', ['phrases' => Sujet::all()->random(1)]);
    }
    // SAUVEGARDE LE SUJET REMPLIT DANS L'INPUT DANS LA BDD
    public function save(Request $request){
        DB::table('sujets')->insert([
            'title' => $request->input('input'),
            'status'=> 0,
        ]);
        return redirect('/redirect');
    }

    //     public function update($id){
    //         $id= 'name';
    //         DB::table('sujets')
    //         ->where('id', $id)
    //         ->update(['status' => 1]);
    //     return redirect()->route('roulette')->with('Traité');
    // }

//     public function update(Request $requestID){
//         $id= $requestID->input("name");
//         DB::table('sujets')
//         ->where('id', $id)
//         ->update(['status' => 1]);
//     // return redirect()->route('roulette')->with('Traité');
// }

}
