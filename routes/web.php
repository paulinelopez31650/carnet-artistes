<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// ROUTES POUR AFFICHER LES SUJETS
Route::get('/roulette', [App\Http\Controllers\SujetsController::class, 'afficheLeSujet'])->name('roulette');
Route::get('/', [App\Http\Controllers\SujetsController::class, 'afficheLesSujets'])->name('roulette');

// ROUTE POUR ENREGISTRER INPUT DANS BDD
Route::post('/welcome',[App\Http\Controllers\SujetsController::class,'save'])->name('sauvegarde');

// ROUTE POUR MODIFIER DONNEE DANS BDD
Route::post('/roulette',[App\Http\Controllers\SujetsController::class,'update'])->name('update');

// ROUTE HOME
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/welcome', [App\Http\Controllers\HomeController::class, 'indexWelcome'])->name('welcome');
Route::get('/redirect', [App\Http\Controllers\HomeController::class, 'indexRedirect'])->name('redirect');

Auth::routes();