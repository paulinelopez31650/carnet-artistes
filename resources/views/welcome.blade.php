
 @extends('layouts.app')
 @section('content')
    <body class="antialiased">
        <h1 class="text-center">Bienvenue sur le site de l'association </h1>
        <form action="{{route('sauvegarde')}}" method="POST">
            @csrf
            <h3> Proposer un sujet </h3>
            <div class="input-group mb-3">
                <span class="input-group-text" id="id">Sujet</span>
                <input type="text" class="form-control" id="input" name="input" 
                aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                <button type="submit" class="btn btn-primary">Valider</button>
            </div>
        </form>
        <h3>Les sujets à venir</h3>
        <div>
            @foreach ($sujets as $sujet)
            <ul class="list-group">
                <li class="list-group-item">{{$sujet->title}}</li>
            </ul>
            @endforeach
        </div>
        @endsection
    </body>
</html>
